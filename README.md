# GIT

## Configuration

System wide configuration
```
$ git config --system
```

User configuration
```
$ git config --global
```

Project configuration
```
$ git config --local
```

### View the settings

View all the configurations and all the settings
```
$ git config --list --show-origin
```

View the value of a specific key
```
$ git config <key>
```

### Your identity

```
$ git config --global user.name <user>
$ git config --global user.email <email>
```

### The editor

```
$ git config --global core.editor <editor>
```

### Default branch name

```
$ git config --global init.defaultBranch <branch>
```

### Alias

```
$ git --global alias.<name> <command>
```

## Start a new repo

Init an empty repo
```
$ git init
```

Clone an existing repo
```
$ git clone <url>
```

## Check the status of the files

```
$ git status
```

Short format of status
```
$ git status --short [-s]
```

## Tracking files

```
$ git add <files>
```

## View changes

View changes between working tree and index
```
$ git diff
```

View changes between index and commit
```
$ git diff --staged [--cached]
```

## Commit changes

```
$ git commit [-m <msg>] [--amend]
```

## Remove files

```
$ git rm
```

Remove file from index
```
$ git rm --cached <file>
```

## Moving files

```
$ git mv <file> <file>
```

## View the proyect history

```
$ git log
```

### Options to the log command

- \-p | \-\-patch: difference patch introduced in each commit
- \-\-stat: stats for each commit
- \-\-pretty=oneline|short|full|fuller
- \-\-graph
- \-n: show the last n commits

## Undo changes

Remove files from index
```
$ git restore --staged <file>
```

Unmodify file
```
$ git restore <file>
```

## Remotes

Show remotes
```
$ git remote [-v]
```

Show more info of the remote
```
$ git remote show <remote>
```

Add a remote
```
$ git remote add <shortname> <url>
```

Get data from the remote
```
$ git fetch <remote>
```

Get data and merge from the remote
```
$ git pull <remote>
```

Push data to a remote
```
$ git push <remote> <branch>
```

## Tags

List the tags
```
$ git tag
```

Create annotated tag
```
$ git tag -a <name> -m <msg> [<commit>]
```

Create lightweight tag
```
$ git tag <name> [<commit>]
```

Show tag
```
$ git show <tag>
```

Push tag
```
$ git push <remote> <tag>
```

Delete local tag
```
$ git tag -d <tag>
```

Delete remote tag
```
$ git push origin --delete <tag>
```